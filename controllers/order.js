const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');





// [ADDITIONAL REQUIREMENTS] Controller function to checkout cart items
module.exports.checkoutProducts = async (req, res) => {
  try {
    const { productId, quantity } = req.body;
    const userId = req.user.id;

    const user = await User.findById(userId);
    if (user.isAdmin) {
      return res.status(403).json(false);
    }

    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json(false);
    }

    const price = product.price;
    const totalAmount = price * quantity;

    const order = new Order({
      userId,
      products: [{ productId, quantity, price }],
      totalAmount
    });

    await order.save();

    return res.json(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: err.message });
  }
};


// [MINIMUM REQUIREMENTS] Controller function to purchase products
module.exports.purchaseProducts = async (req, res) => {
  try {
    const itemsToPurchase = req.body.items; // Extract the items from the request body
    const userId = req.user.id;

    const user = await User.findById(userId);
    if (user.isAdmin) {
      return res.status(403).json(false);
    }

    // Loop through each item in the itemsToPurchase array and create an order for each product
    for (const item of itemsToPurchase) {
      const { productId, quantity } = item;
      const product = await Product.findById(productId);
      if (!product) {
        return res.status(404).json(false);
      }

      const price = product.price;
      const totalAmount = price * quantity;

      const order = new Order({
        userId,
        products: [{ productId, quantity, price }],
        totalAmount
      });

      await order.save();
    }

    return res.json(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: error.message });
  }
};





// [STRETCH GOAL] Controller function to retrieve authenticated user orders
module.exports.retrieveUserOrders = async (req, res) => {
  try {
    const userId = req.user.id;

    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json(false);
    }

    const orders = await Order.find({ userId });

    return res.status(200).json({ orders });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: err.message });
  }
};





// [STRETCH GOAL] Controller function to retrieve all orders (admin only)
module.exports.retrieveAllOrders = async (req, res) => {
  try {
    
    const orders = await Order.find();
    return res.status(200).json({ orders });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: error.message });
  }
};





