const Product = require("../models/Product");
const User = require("../models/User")





// [MINIMUM REQUIREMENTS] ADD PRODUCT (ONLY ADMIN CAN ACCESS)
module.exports.addProduct = (req, res) => {
  const { name, description, price, image } = req.body;

  Product.findOne({ name })
    .then((existingProduct) => {
      if (existingProduct) {
        return res.send(false);
      }

      const newProduct = new Product({
        name,
        description,
        price,
        image,
      });

      return newProduct.save()
        .then(() => res.send(true))
        .catch((error) => res.send(error));
    })
    .catch((error) => res.send(error));
};






// [MINIMUM REQUIREMENTS] RETRIEVE ALL PRODUCTS (ADMIN ONLY) 
module.exports.getAllProduct = (req, res) => {
    return Product.find({}).then(result => {
        return res.send(result);
    })
    .catch(err => res.send(err))
};





// [MINIMUM REQUIREMENTS] RETRIEVE ALL ACTIVE PRODUCTS 
module.exports.getAllActive = (req, res) => {
    return Product.find({ isActive: true }).then(result => {
        return res.send(result)
    })
    .catch(err => res.send(err))
};





// [MINIMUM REQUIREMENTS] RETRIEVE SINGLE PRODUCTS 
module.exports.getProduct = (req, res) => {
    return Product.findById(req.params.productId).then(result => {
        return res.send(result)
    })
    .catch(err => res.send(err))
};





// [MINIMUM REQUIREMENTS] UPDATE PRODUCT INFORMATION (ADMIN ONLY)
module.exports.updateProduct = (req, res) => {

    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        image: req.body.image
    }

    return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
        .then((product, error) => {

        if(error) {
            return res.send(false);

        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};





// [MINIMUM REQUIREMENTS] ARCHIVE PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (req, res) => {

    let archivedProduct = {
        isActive: false
    }
    return Product.findByIdAndUpdate(req.params.productId, archivedProduct)
    .then((course, error) => {

        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};





// [MINIMUM REQUIREMENTS] ACTIVATE PRODUCT (ADMIN ONLY)
module.exports.activateProduct = (req, res) => {

    let activatedProduct = {
        isActive: true
    }

    return Product.findByIdAndUpdate(req.params.productId, activatedProduct)
    .then((course, error) => {

        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
}





// [ADDITIONAL FEATURES] SEARCH PRODUCT BY PRODUCT NAME
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    }); 

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json(false);
  }
};





// [ADDITIONAL FEATURES] SEARCH FOR PRODUCT BASED ON PRICE RANGE
module.exports.searchProductByPriceRange = async (req, res) => {
  const { minPrice, maxPrice } = req.body;

  try {
    const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.json({ message: err.message });
  }
};





// [ADDITIONAL REQUIREMENTS] HARD DELETE A PRODUCT
module.exports.deleteProduct = async (req, res) => {
  try {
    const { productId } = req.params;

    const product = await Product.findById(productId);
    if (!product) {
      return res.json(false);
    }

    await Product.findByIdAndRemove(productId);
    return res.json(true);
  } catch (error) {
    console.error(error);
    return res.json({ message: err.message });
  }
};
