// Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require('bcrypt');
const auth = require('../auth');





// [MINIMUM REQUIREMENTS] START OF REGISTER NEW USER
module.exports.registerUser = (req, res) => {
  const { firstName, lastName, mobileNo, address, email, password } = req.body;

  User.findOne({ email })
    .then(existingUser => {
      if (existingUser) {
        return res.send(false);
      }

      const newUser = new User({
        firstName,
        lastName,
        email,
        mobileNo,
        address,
        password: bcrypt.hashSync(password, 10)
      });

      newUser.save()
        .then(savedUser => {

          return res.send(true);
        })
        .catch(error => {

          return res.send(false);
        });
    })
    .catch(error => {
      return res.send({ message: err.message });
    });
};





// [MINIMUM REQUIREMENTS] START OF LOGIN USER
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		
		if(result == null) {
			return false
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {
				
				return res.send({access: auth.createAccessToken(result)})
			} else {

				return res.send(false);
			}
		}
	}).catch(err => res.send(err));
};





// [MINIMUM REQUIREMENTS] START OF RETIEVING USER DETAILS
module.exports.getProfile = (req, res) => {
    return User.findById(req.user.id).then(result => {
            
            result.password = "";
            
            return res.send(result); 
            
        }).catch(err => res.send(err));
};

module.exports.getAllUsers = (req, res) => {
    return User.find({}).then(result => {
        return res.send(result);
    })
    .catch(err => res.send(err))
};





// [STRETCH GOAL] START OF UPDATE USER TO BE ADMIN
module.exports.updateUserToBeAdmin = async (req, res) => {
  const userId = req.params.userId; // Retrieve userId from URL parameters

  try {
    const user = await User.findById(userId);

    if (!user) {
      return res.json(false);
    }

    user.isAdmin = true;
    await user.save();

    res.json(true);
  } catch (error) {
    console.error(error);
    res.json({ message: error.message });
  }
};





// [ADDITIONAL FEATURES] START OF UPDATE USER TO BE REMOVE AS ADMIN
module.exports.removeUserToBeAdmin = async (req, res) => {
  const userId = req.params.userId; // Retrieve userId from URL parameters

  try {
    const user = await User.findById(userId);

    if (!user) {
      return res.json(false);
    }

    user.isAdmin = false;
    await user.save();

    res.json(true);
  } catch (error) {
    console.error(error);
    res.json({ message: error.message });
  }
};




// [ADDITIONAL FEAUTURES] RESETTING PASSWORD
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user;

    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await User.findByIdAndUpdate(id, { password: hashedPassword });

    res.json(true);
  } catch (error) {
    console.error(error);
    res.json({ message: err.message });
  }
};





// [ADDITIONAL FEAUTURES] UPDATING PROFILE
module.exports.updateProfile = async (req, res) => {
  try {
    const userId = req.user.id;

    const { firstName, lastName, email, address } = req.body;

    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, email, address },
      { new: true, select: 'firstName lastName email address' }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.json({ message: err.message });
  }
};





// [ADDITIONAL FEAUTURES] CHECK IF EMAIL EXIST
module.exports.checkEmailExists = (req, res) => {
  const { email } = req.body;

  User.find({ email })
    .then(result => {
      if (result.length > 0) {
        res.json(true);
      } else {
        res.json(false);
      }
    })
    .catch(error => {
      console.error(error);
      res.json({ message: err.message });
    });
};





// [STRETCH GOAL] ADD TO CART
let cart = {}; // Temporary cart storage
module.exports.addToCart = async (req, res) => {
  const { productId, quantity } = req.body;

  try {
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json(false);
    }

    const userId = req.user.id;

    if (!cart[userId]) {
      cart[userId] = {};
    }

    if (cart[userId][productId]) {
      cart[userId][productId].quantity += quantity;
    } else {
      cart[userId][productId] = {
        product,
        quantity
      };
    }

    return res.send(true);
  } catch (err) {
    return res.send({ message: err.message });
  }
};





// [STRETCH GOAL] GET CART ITEMS
module.exports.getCartItems = async (req, res) => {
  try {
    const userId = req.user.id;

    if (!cart[userId]) {
      return res.send({ cartItems: [] });
    }

    const cartItems = Object.values(cart[userId]).map(item => ({
      productId: item.product._id,
      productName: item.product.name,
      quantity: item.quantity,
      price: item.product.price,
      subtotal: item.quantity * item.product.price
    }));

    return res.send({ cartItems });
  } catch (err) {
    return res.send({ message: err.message });
  }
};





// [STRETCH GOAL] GET CART TOTAL
module.exports.getCartTotal = async (req, res) => {
  try {
    const userId = req.user.id;

    if (!cart[userId]) {
      return res.send({ total: 0 });
    }

    let total = 0;

    Object.values(cart[userId]).forEach(item => {
      total += item.quantity * item.product.price;
    });

    return res.send({ total });
  } catch (err) {
    return res.send({ message: err.message });
  }
};





// [STRETCH GOAL] CHANGE QUANTITY IN CART
module.exports.changeQuantityInCart = async (req, res) => {
  const { productId, quantity } = req.body;
  const userId = req.user.id;

  try {
    if (!cart[userId] || !cart[userId][productId]) {
      return res.status(404).json(false);
    }

    cart[userId][productId].quantity = quantity;

    return res.send(true);
  } catch (err) {
    return res.send({ message: err.message });
  }
};





// [STRETCH GOAL] REMOVE ITEM FROM CART
module.exports.removeFromCart = async (req, res) => {
  const productId = req.params.productId;
  const userId = req.user.id;

  try {
    if (!cart[userId] || !cart[userId][productId]) {
      return res.status(404).json(false);
    }

    delete cart[userId][productId];

    return res.send(true);
  } catch (err) {
    return res.send({ message: err.message });
  }
};





// [ADDITIONAL FEAUTURES] DELETE USER PROFILE
exports.deleteUser = async (req, res) => {
  try {
    const { userId } = req.params;

    const user = await User.findById(userId);
    if (!user) {
      return res.json(false);
    }

    await User.findByIdAndRemove(userId);
    return res.json(true);
  } catch (error) {
    console.error(error);
    return res.json({ message: err.message });
  }
};





