const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

// destructure the auth file:
// here we use the verrify and verifyAdmin as auth middlewares.
const { verify, verifyAdmin } = auth;



const router = express.Router();



// [MINIMUM REQUIREMENTS] USER REGISTRATION
router.post("/register", userController.registerUser); 

// [MINIMUM REQUIREMENTS] USER LOGIN
router.post("/login", userController.loginUser);

// [MINIMUM REQUIREMENTS] RETRIEVE USER DETAILS
router.get("/details", verify, userController.getProfile);

router.get("/getAllUsers",  userController.getAllUsers);




// [SECTION]-------START OF STRETCH GOAL-------[SECTION]

// [STRETCH GOAL] UPDATE USER TO BE ADMIN
router.put('/updateAdmin/:userId', verify, verifyAdmin, userController.updateUserToBeAdmin);



// [ADDITIONAL FEATURES] REMOVE USER TO BE ADMIN
router.put('/removeAdmin/:userId', verify, verifyAdmin, userController.removeUserToBeAdmin);


// [ADDITIONAL FEATURES] RESETTING THE PASSWORD
router.put('/reset-password', verify, userController.resetPassword);

// [ADDITIONAL FEATURES] UPDATING PROFILE
router.put('/profile', verify, userController.updateProfile);

// [ADDITIONAL FEAUTURES] CHECK IF EMAIL EXIST
router.post("/checkEmail", userController.checkEmailExists);

// [ADDITIONAL FEAUTURES] DELETE USER PROFILE
router.delete('/:userId/delete', verify, verifyAdmin, userController.deleteUser);



// [STRETCH GOAL] ADD PRODUCT TO CART
router.post("/cart/add", verify, userController.addToCart);

// [STRETCH GOAL] GET CART ITEMS
router.get("/cart/items", verify, userController.getCartItems);

// [STRETCH GOAL] GET CART TOTAL
router.get("/cart/total", verify, userController.getCartTotal);

// [STRETCH GOAL] UPDATE PRODUCT QUANTITY IN CART
router.put("/cart/:productId", verify, userController.changeQuantityInCart);

// [STRETCH GOAL] REMOVE PRODUCT FROM CART
router.delete("/cart/:productId", verify, userController.removeFromCart);


module.exports = router;