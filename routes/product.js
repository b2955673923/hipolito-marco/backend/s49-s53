const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');


const { verify, verifyAdmin } = auth;

const router = express.Router();


// [MINIMUM REQUIREMENTS]-------START OF MINIMUM REQUIREMENTS-------[MINIMUM REQUIREMENTS]


// [MINIMUM REQUIREMENTS] Create a product functionality
router.post("/", verify, verifyAdmin,productController.addProduct);

// [MINIMUM REQUIREMENTS] Route for retrieving all the product for admin
router.get("/all", productController.getAllProduct);

// [MINIMUM REQUIREMENTS] Route for retrieving all the ACTIVE product for all the users.
router.get("/", verify, productController.getAllActive);



// [ADDITIONAL FEATURES] SEARCH PRODUCT BY PRODUCT NAME
router.post('/search', productController.searchProductsByName);

// [ADDITIONAL FEATURES] SEARCH FOR PRODUCT BASED ON PRICE RANGE
router.post('/search-price-range', productController.searchProductByPriceRange);

// [ADDITIONAL FEATURES] HARD DELETE PRODUCT
router.delete('/:productId/delete', verify, verifyAdmin, productController.deleteProduct);



// [MINIMUM REQUIREMENTS] Route for retrieving single product
router.get("/:productId", productController.getProduct);

// [MINIMUM REQUIREMENTS]  Route for updating a course (Admin)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// [MINIMUM REQUIREMENTS] Route for Archiving Course (Admin)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// [MINIMUM REQUIREMENTS] Route for Activating Course (Admin)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);







module.exports = router;