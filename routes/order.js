// Dependencies and Modules
const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');


// destructure the auth file:
const { verify, verifyAdmin } = auth;


const router = express.Router();


// [MINIMUM REQUIREMENTS] USER PURCHASE ITEMS IN CART
router.post("/purchase", verify, orderController.purchaseProducts);

// [ADITIONAL REQUIREMENTS] USER PURCHASE ITEM
router.post("/checkout", verify, orderController.checkoutProducts);

// [STRETCH GOAL] RETRIEVE AUTHENTICATED USER'S ORDERS
router.get('/user-orders', verify, orderController.retrieveUserOrders);

// [STRETCH GOAL] RETRIEVE ALL ORDERS (ADMIN)
router.get('/all-orders',orderController.retrieveAllOrders);


module.exports = router; 